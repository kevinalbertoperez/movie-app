import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'models/movieDetailModel.dart';
import 'models/movieModel.dart';
import 'package:intl/intl.dart';
import 'models/movieCreditsModel.dart';
import 'models/cast_crew.dart';
import 'models/tmdb.dart';
//const topRatedUrl = "${baseUrl}top_rated?api_key=$apiKey";
//https://api.themoviedb.org/3/movie/299534?api_key=996ee70ca439b98cf33778c750a60f9d&language=en-US

class MovieDetails extends StatefulWidget {
  final Results movie;
  MovieDetails({this.movie});
  @override
  _MovieDetail createState() => new _MovieDetail();
}

class _MovieDetail extends State<MovieDetails> {
  String movieDetailUrl;
  MovieDetailModel movieDetails;
  MovieCredits movieCredits;
  String movieCreditsUrl;
  bool isLoading = true;
  List<CastCrew> castCrew = new List<CastCrew>();

  @override
  initState() {
    super.initState();
    movieDetailUrl = "${Tmdb.baseUrl}${widget.movie.id}?api_key=${Tmdb.apiKey}";
    movieCreditsUrl = "${Tmdb.baseUrl}${widget.movie.id}/credits?api_key=${Tmdb.apiKey}";
    _fetchMovieDetails();
    _fetchMovieCredits();
  }

  void _fetchMovieDetails() async {
    var response = await http.get(movieDetailUrl);
    var decodeJson = jsonDecode(response.body);
    setState(() {
      movieDetails = MovieDetailModel.fromJson(decodeJson);
    });
  }

  void _fetchMovieCredits() async {
    var response = await http.get(movieCreditsUrl);
    var decodeJson = jsonDecode(response.body);
    movieCredits = MovieCredits.fromJson(decodeJson);
    movieCredits.cast.forEach((f) => castCrew.add(CastCrew(
        id: f.id,
        name: f.name,
        subName: f.character,
        imagePath: f.profilePath,
        personType: "actors")));
    movieCredits.crew.forEach((f) => castCrew.add(CastCrew(
        id: f.id,
        name: f.name,
        subName: f.job,
        imagePath: f.profilePath,
        personType: "crew")));
    setState(() {
      isLoading = false;
    });
  }

  String _getMovieDuration(int runtime) {
    if (runtime == null) {
      return "No data";
    } else {
      double movieDuration = runtime / 60;
      int movieMinutes = ((movieDuration - movieDuration.floor()) * 60).round();
      return "${movieDuration.floor()}:${movieMinutes}min";
    }
  }

  Widget _buidCastCrewContent(String personType) => Container(
        height: 116.0,
        padding: EdgeInsets.only(top: 8.0),
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.1)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
              child: Text(
                personType=="crew"?"Crews":"Actors",
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[400]),
              ),
            ),
            Flexible(
              child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: isLoading
                      ? <Widget>[
                          Center(
                            child: CircularProgressIndicator(),
                          )
                        ]
                      : castCrew
                          .where((c) => c.personType == personType)
                          .map((f) => Padding(
                                padding: EdgeInsets.only(left: 4.0),
                                child: Container(
                                  width: 65.0,
                                  child: Column(
                                    children: <Widget>[
                                      CircleAvatar(
                                        radius: 28.0,
                                        backgroundImage: f.imagePath != null
                                            ? NetworkImage(
                                                "${Tmdb.baseImagesUrl}w154${f.imagePath}")
                                            : AssetImage(
                                                "assets/img/nobody.jpg"),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 4.0),
                                        child: Text(
                                          f.name,
                                          style: TextStyle(
                                              fontSize: 8.0,
                                              fontWeight: FontWeight.bold),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 4.0),
                                        child: Text(
                                          f.subName,
                                          style: TextStyle(
                                              fontSize: 8.0,
                                              fontWeight: FontWeight.bold),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ))
                          .toList()),
            )
          ],
        ),
      );

  Widget build(BuildContext context) {
    final moviePoster = Container(
      height: 350.0,
      padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Center(
        child: Card(
          elevation: 10.0,
          child: Hero(
            tag: widget.movie.heroTag,
            child: Image.network(
              "${Tmdb.baseImagesUrl}w342${widget.movie.posterPath}",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );

    final movieTitle = Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 4.0, bottom: 8.0),
        child: Text(
          widget.movie.title,
          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );

    final generesList = Container(
      height: 25.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: movieDetails == null
              ? []
              : movieDetails.genres
                  .map((g) => Padding(
                      padding: const EdgeInsets.only(right: 6.0),
                      child: FilterChip(
                        backgroundColor: Colors.grey[600],
                        labelStyle: TextStyle(fontSize: 10.0),
                        label: Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(g.name),
                        ),
                        onSelected: (o) {},
                      )))
                  .toList(),
        ),
      ),
    );

    final movieTickets = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          movieDetails != null ? _getMovieDuration(movieDetails.runtime) : "",
          style: TextStyle(fontSize: 11.0),
        ),
        Container(
          height: 20.0,
          width: 1.0,
          color: Colors.white70,
        ),
        Text(
          "Relase Data ${DateFormat('dd-MM-yyyy').format(DateTime.parse(widget.movie.releaseDate))}",
          style: TextStyle(fontSize: 11.0),
        ),
        RaisedButton(
          shape: StadiumBorder(),
          elevation: 5.0,
          color: Colors.red[700],
          child: Text("Like"),
          onPressed: () {

          },
        )
      ],
    );

    final middleContent = Container(
      padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 2.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(),
          generesList,
          Divider(),
          Text("Sinopsis",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.grey[300])),
          SizedBox(
            height: 10.0,
          ),
          Text(
            widget.movie.overview,
            style: TextStyle(fontSize: 11.0, color: Colors.grey[300]),
          ),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );

    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Movie Apps",
            style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          elevation: 0.0,
        ),
        body: ListView(
          children: <Widget>[
            moviePoster,
            movieTitle,
            movieTickets,
            middleContent,
            _buidCastCrewContent("actors"),
            _buidCastCrewContent("crew"),
          ],
        ));
  }
}
