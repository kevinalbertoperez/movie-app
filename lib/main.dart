import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'models/movieModel.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:intl/intl.dart';
import 'movieDetails.dart';
import 'models/tmdb.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Movie App",
      theme: ThemeData.dark(),
      home: MyMovieApp(),
    ));

class MyMovieApp extends StatefulWidget {
  @override
  _MyMovieApp createState() => _MyMovieApp();
}

class _MyMovieApp extends State<MyMovieApp> {
  Movie nowPlayingMovies;
  Movie upcomingMovies;
  Movie popularMovies;
  Movie topRatedMovies;
  int heroTag = 0;
  int _currentIndex = 0;

  //Pagination
  int _pageNumberUpcoming = 1;
  int _totalItemsUpcoming = 0;
  int _pageNumberPopular = 1;
  int _totalItemsPopular = 0;
  int _pageNumberTopRated = 1;
  int _totalItemsTopRated = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchNowPlayingMovies();
    _fetchUpcomingMovies();
    _fetchPopularMovies();
    _fetchTopRatedMovies();
  }

  void _fetchNowPlayingMovies() async {
    var response = await http.get(Tmdb.nowPlayingUrl);
    var decodeJson = jsonDecode(response.body);
    setState(() {
      nowPlayingMovies = Movie.fromJson(decodeJson);
      print(decodeJson);
    });
  }

  void _fetchUpcomingMovies() async {
    var response = await http.get(
        "${Tmdb.baseUrl}now_playing?api_key=${Tmdb.apiKey}&page=$_pageNumberUpcoming");
    var decodeJson = jsonDecode(response.body);
    upcomingMovies == null
        ? upcomingMovies = Movie.fromJson(decodeJson)
        : upcomingMovies.results.addAll(Movie.fromJson(decodeJson).results);
    setState(() {
      _totalItemsUpcoming = upcomingMovies.results.length;
    });
  }

  void _fetchPopularMovies() async {
    var response = await http.get(
        "${Tmdb.baseUrl}popular?api_key=${Tmdb.apiKey}&page=$_pageNumberPopular");
    var decodeJson = jsonDecode(response.body);
    popularMovies == null
        ? popularMovies = Movie.fromJson(decodeJson)
        : popularMovies.results.addAll(Movie.fromJson(decodeJson).results);
    setState(() {
      _totalItemsPopular = popularMovies.results.length;
    });
  }

  void _fetchTopRatedMovies() async {
    var response =
        await http.get(Tmdb.topRatedUrl + "&page=$_pageNumberTopRated");
    var decodeJson = jsonDecode(response.body);
    topRatedMovies == null
        ? topRatedMovies = Movie.fromJson(decodeJson)
        : topRatedMovies.results.addAll(Movie.fromJson(decodeJson).results);
    setState(() {
      _totalItemsTopRated = topRatedMovies.results.length;
    });
  }

  Widget _buildCarouselSlider() => CarouselSlider(
      autoPlay: false,
      height: 240.0,
      viewportFraction: 0.5,
      items: nowPlayingMovies == null
          ? <Widget>[
              Center(
                child: CircularProgressIndicator(),
              )
            ]
          : nowPlayingMovies.results
              .map((movieItem) => _buildMovieItem(movieItem))
              .toList());

  Widget _buildMovieItem(Results movieItem) {
    heroTag += 1;
    movieItem.heroTag = heroTag;
    return Material(
      elevation: 15.0,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MovieDetails(movie: movieItem)));
        },
        child: Hero(
            tag: heroTag,
            child: movieItem.posterPath != null
                ? Image.network(
                    "${Tmdb.baseImagesUrl}w154${movieItem.posterPath}")
                : Image.asset(
                    "assets/img/empty.jpg",
                    fit: BoxFit.cover,
                  )),
      ),
    );
  }

  Widget _buildMovieListViewItem(Results movieItem) => Material(
        child: Container(
          width: 128.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(
                    6.0,
                  ),
                  child: _buildMovieItem(movieItem)),
              Padding(
                padding: EdgeInsets.only(left: 6.0, top: 2.0),
                child: Text(
                  movieItem.title,
                  style: TextStyle(fontSize: 8.0),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 6.0, top: 2.0),
                child: Text(
                  DateFormat("yyyy")
                      .format(DateTime.parse(movieItem.releaseDate)),
                  style: TextStyle(fontSize: 8.0),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buidMoviesListView(Movie movie, String movieListTitle, String type) =>
      Container(
        height: 258.0,
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.4)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 7.0, bottom: 7.0),
              child: Text(
                movieListTitle,
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[400]),
              ),
            ),
            Flexible(
                child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: type == "upcomming"
                  ? _totalItemsUpcoming
                  : type == "toprated"
                      ? _totalItemsTopRated
                      : _totalItemsPopular,
              itemBuilder: (BuildContext context, int index) {
                if (index >= movie.results.length - 1) {
                  type == "upcomming"
                      ? _pageNumberUpcoming++
                      : type == "toprated"
                          ? _pageNumberTopRated++
                          : _pageNumberPopular++;
                  type == "upcomming"
                      ? _fetchUpcomingMovies()
                      : type == "toprated"
                          ? _fetchTopRatedMovies()
                          : _fetchPopularMovies();
                }
                return Padding(
                  padding: EdgeInsets.only(left: 6.0, right: 2.0),
                  child: _buildMovieListViewItem(movie.results[index]),
                );
              },
            )),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          fixedColor: Colors.lightBlue,
          onTap: (int index) {
            setState(() {
               _currentIndex = index;
               print(index);
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.local_movies), title: Text("All Movies")),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite), title: Text("Favorites")),
            BottomNavigationBarItem(
                icon: Icon(Icons.tag_faces), title: Text("Tickets")),
          ],
        ),
        body: NestedScrollView(
          body: ListView(
            children: <Widget>[
              _buidMoviesListView(upcomingMovies,
                  "Comming Soon (Page $_pageNumberUpcoming)", "upcomming"),
              _buidMoviesListView(popularMovies,
                  "Popular (Page $_pageNumberPopular)", "popular"),
              _buidMoviesListView(topRatedMovies, "Top Rated  (Page $_pageNumberTopRated)", "toprated"),
            ],
          ),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                title: Center(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      "Now Playing",
                      style: TextStyle(
                          color: Colors.grey[400],
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                expandedHeight: 290.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  background: Stack(
                    children: <Widget>[
                      Container(
                        child: Image.network(
                          "https://images.wallpaperscraft.com/image/black_light_dark_figures_73356_1920x1080.jpg",
                          fit: BoxFit.cover,
                          width: 1000.0,
                          colorBlendMode: BlendMode.dstATop,
                          color: Colors.blue.withOpacity(0.5),
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 35.0),
                          child: _buildCarouselSlider())
                    ],
                  ),
                ),
              )
            ];
          },
        ),
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            "My Movie App",
            style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            )
          ],
        ));
  }
}
