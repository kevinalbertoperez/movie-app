class Tmdb{
  static const apiKey = "996ee70ca439b98cf33778c750a60f9d";
  static const baseUrl = "https://api.themoviedb.org/3/movie/";
  static const baseImagesUrl = "https://image.tmdb.org/t/p/";
  static const nowPlayingUrl = "${baseUrl}now_playing?api_key=$apiKey";
  static const upcomingUrl = "${baseUrl}upcoming?api_key=$apiKey";
  static const popularUrl = "${baseUrl}popular?api_key=$apiKey";
  static const topRatedUrl = "${baseUrl}top_rated?api_key=$apiKey";

}